#!/bin/bash

# server
server=ldas-pcdev5.ligo.caltech.edu
gridftp_port=2811
test_file=test_file.dat
remote_test_file=test_file_1.dat

# generate grid proxy
ligo-proxy-init $1 || exit ${?}
echo

# view grid proxy
grid-proxy-info || exit ${?}
echo

# gsissh
printf "testing gsissh: "
gsissh ${server} hostname &> /dev/null || exit ${?}
printf "done\n"

# gsiscp
printf "testing gsiscp: "
gsiscp ${test_file} ${server}:./${remote_test_file} &> /dev/null || exit ${?}
gsiscp ${server}:./${remote_test_file} ${remote_test_file} &> /dev/null || exit ${?}
gsissh ${server} rm -f ${remote_test_file} &> /dev/null || exit ${?}
rm ${remote_test_file} || exit ${?}
printf "done\n"

# globus-url-copy
printf "testing globus-url-copy: "
cwd=`pwd`
globus-url-copy file://${cwd}/${test_file} gsiftp://${server}:${gridftp_port}/tmp/${remote_test_file} &> /dev/null || exit ${?}
globus-url-copy gsiftp://${server}:${gridftp_port}/tmp/${remote_test_file} file://${cwd}/${remote_test_file} &> /dev/null || exit ${?}
gsissh ${server} rm -f /tmp/${remote_test_file} &> /dev/null || exit ${?}
rm ${remote_test_file} || exit ${?}
printf "done\n"

# globus-job-run
#printf "testing globus-job-run (fork): "
#globus-job-run ${server}/jobmanager-fork /bin/hostname &> /dev/null exit ${?}
#printf "done\n"
#printf "testing globus-job-run (condor): "
#globus-job-run ${server}/jobmanager-condor /bin/hostname &> /dev/null exit ${?}
#printf "done\n"
