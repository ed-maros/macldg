#!/bin/bash

productsign --sign "Developer ID Installer: Robert Mercer (6RN65VR4WQ)" $1 $2
openssl sha1 $2 > `basename $2 .pkg`.chk
