#!/usr/bin/env python
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright (C) 2013-2015 Adam Mercer <adam.mercer@ligo.org>

import sys
import optparse
import os
import getpass
import grp
import pwd
import subprocess

# script metadata
__author__ = 'Adam Mercer <adam.mercer@ligo.org>'
__version__ = '1.0.6'

#
# helper methods
#

# parse command line options
def parse_options():
  # define program usage
  usage = '%prog [options]'

  # define option parser
  parser = optparse.OptionParser(usage=usage, version=__version__)
  parser.add_option('--stable', action='store_true', default=False,
      help='switch to stable channel')
  parser.add_option('--beta', action='store_true', default=False,
      help='switch to beta channel')
  parser.add_option('--quiet', action='store_true', default=False,
      help='run in quiet mode')
  parser.add_option('--reset', action='store_true', default=False,
      help='reset channel status')

  # parse command line options
  opts, args = parser.parse_args()

  # check for any positional arguments
  if args:
    parser.error('incorrect number of command line options specified')

  # stable and beta cannot both be set
  if opts.stable and opts.beta:
    parser.error('cannot specify both --stable and --beta')

  # define channel
  if opts.stable:
    opts.channel = 'stable'
  elif opts.beta:
    opts.channel = 'beta'
  else:
    opts.channel = None

  # return parsed arguments
  return opts


# return channel config file
def get_channel_config_file():
  try:
    return '%s/etc/channel.conf' % os.environ['GLOBUS_LOCATION']
  except KeyError:
    sys.exit('Error: GLOBUS_LOCATION not defined')


# return current channel
def get_current_channel():
  try:
    with open(get_channel_config_file(), 'r') as f:
      return f.readline().strip()
  except IOError:
    # channel file not present, stable by definition
    return 'stable'


# check that current user is an administrator
def valid_user():
  # get groups that current user belongs to
  user = getpass.getuser()
  groups = [g.gr_name for g in grp.getgrall() if user in g.gr_mem]
  try:
    # append gid to list of groups
    gid = pwd.getpwnam(user).pw_gid
    groups.append(grp.getgrgid(gid).gr_name)
  except KeyError:
    print('Warning: Unable to identify gid "%d"' % gid)
    pass

  # is user a member of the 'admin' group
  if 'admin' in groups:
    return True
  else:
    return False


# return output from given command
def run_command(command):
  # run command and get output
  p = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
  out, _ = p.communicate()

  # convert byte objects to strings, if appropriate
  if not isinstance(out, str) and sys.version_info >= (3,):
    out = str(out, encoding='utf8')

  # check for failure
  if p.returncode != 0:
    raise RuntimeError('failed to run "%s"' % " ".join(command))

  return out.strip()


# return path to binary from environment
def get_binary_path(fpath):
  return ['%s/%s' % (x, fpath) for x in os.environ['PATH'].split(':')
      if os.path.exists('%s/%s' % (x, fpath))][0]


# return version of currently installed ldg
def get_current_ldg_version():
  try:
    ldg_version_script = get_binary_path('ldg-version')
    return run_command([ldg_version_script, '--ldg-only'])
  except IndexError:
    sys.exit('Error: Unable to locate "ldg-version" script')
  except RuntimeError:
    sys.exit('Error: Unable to determine current LDG version')


# return latest version of ldg
def get_latest_ldg_version():
  try:
    ldg_check_script = get_binary_path('ldg-check')
    return run_command([ldg_check_script, '--latest-version'])
  except IndexError:
    sys.exit('Error: Unable to locate "ldg-check" script')
  except RuntimeError:
    sys.exit('Error: Unable to determine latest LDG version')


# convert version string to list
def version_to_list(version):
  try:
    return [int(x) for x in version.split('.')]
  except ValueError:
    sys.exit('Error: Unable to parse version')


# set channel
def set_channel(options, channel):
  # get current channel
  if channel == get_current_channel():
    if not options.quiet:
      print('You are already on the %s channel' % channel)
    return True

  # check that we are running as an admin user
  if not valid_user():
    sys.exit('Error: Not running as an administrator')

  # set channel
  try:
    # convert to bytes objects if appropriate
    if isinstance(channel, str) and sys.version_info >= (3,):
      channel = bytes(channel, encoding='utf8')

    # update channel.conf
    command=['sudo', 'tee', get_channel_config_file()]
    p = subprocess.Popen(command, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, _ = p.communicate(input=channel)
    if p.returncode != 0:
      raise RuntimeError

    # ensure correct permissions for channel.conf
    command=['sudo', 'chmod', '644', get_channel_config_file()]
    p = subprocess.Popen(command, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, _ = p.communicate()
    if p.returncode != 0:
      raise RuntimeError

    # convert back to string object, if appropriate
    if not isinstance(channel, str) and sys.version_info >= (3,):
      channel = str(channel, encoding='utf8')

    if channel == 'stable':
      # report
      if not options.quiet:
        print('Now on %s channel' % channel)
      # is a downgrade needed
      if version_to_list(get_current_ldg_version()) > version_to_list(get_latest_ldg_version()):
        print('Please run "ldg-upgrade --force" to downgrade to %s release' % channel)
    else:
      if not options.quiet:
        print('Now on %s channel' % channel)

    return True
  except RuntimeError:
    return False


# reset channel status
def reset_channel(options):
  # get channel file
  channel_file = get_channel_config_file()

  # check that we are running as an admin user
  if not valid_user():
    sys.exit('Error: Not running as an administrator')

  # remove file
  try:
    # does file exist, and is it a file
    if os.path.exists(channel_file) and os.path.isfile(channel_file):
      if not options.quiet:
        print('Resetting channel status')
      command=['sudo', 'rm', channel_file]
      p = subprocess.Popen(command, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
      out, _ = p.communicate()
      if p.returncode != 0:
        raise RuntimeError
    else:
      if not options.quiet:
        print('No need to reset channel status')
  except OSError:
    pass
  except RuntimeError:
    sys.exit('Error: Unable to delete channel file')

#
# main program
#

def main():
  # parse command line options
  options = parse_options()

  # reset channel status
  if options.reset:
    reset_channel(options)
    sys.exit()

  if not options.channel:
    # report current channel
    print('Currently on %s channel' % get_current_channel())
  else:
    # set to specified channel
    if not set_channel(options, options.channel):
      sys.exit('Error: Unable to set channel')

  # exit cleanly
  sys.exit()

#
# main program entry point
#

if __name__ == '__main__':
  main()
