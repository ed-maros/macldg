#!/bin/bash

# check if we are running as root
if [ `id -u` != "0" ]; then
    echo "Error: This script requires being run as root."
    exit 1
fi

# stop ldg-check, if running
launchctl list org.ligo.ldg-check &> /dev/null
rc=$?
if [[ $rc == 0 ]]; then
  launchctl unload /Library/LaunchDaemons/org.ligo.ldg-check.plist
fi

# remove ldg-check plist
rm -f /Library/LaunchDaemons/org.ligo.ldg-check.plist

# remove newsyslog configuration
rm -f /etc/newsyslog.d/org.ligo.ldg-check.conf

# remove LDG Client
if [[ -d /opt/ldg ]]; then
  rm -rf /opt/ldg
fi

# remove ldg-check update info/log
rm -f /var/log/ldg-check-update
rm -f /var/log/ldg-check.log
