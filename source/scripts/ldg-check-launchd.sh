#!/bin/bash

export GLOBUS_LOCATION=/opt/ldg
if [ -f ${GLOBUS_LOCATION}/etc/globus-user-env.sh ]; then
  . ${GLOBUS_LOCATION}/etc/globus-user-env.sh
fi

${GLOBUS_LOCATION}/bin/ldg-check --launchd
