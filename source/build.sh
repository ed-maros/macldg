#!/bin/bash

#
# helper functions
#

download_and_checksum () {
  # get arguments
  url=$1
  filename=$2
  checksum=$3

  # does file alreay exist?
  if [ -f ${filename} ]
  then
    # does it have the correct checksum
    sum=(`shasum ${filename}`) || exit ${?}
    if [ "${sum[0]}" != "${checksum}" ]
    then
      rm ${filename} || exit ${?}
    else
      # checksum is good, no need to download again
      echo "no need to download ${filename}"
      return
    fi
  fi

  # download
  echo "downloading: ${filename}"
  curl -L -O ${url}/${filename} || exit ${?}

  # calculate checksum
  sum=(`shasum ${filename}`) || exit ${?}

  # check
  if [ "${sum[0]}" != "${checksum}" ]
  then
    echo "ERROR: Bad checksum for ${filename}"
    echo "       Expecting: ${checksum}"
    echo "       Actual:    ${sum[0]}"
    exit
  fi
}

# tarballs

# pkg-config
pkgconfig_version="0.29.2"
pkgconfig_checksum="76e501663b29cb7580245720edfb6106164fad2b"
pkgconfig_url="http://pkgconfig.freedesktop.org/releases"
pkgconfig_extension="tar.gz"
pkgconfig_filename="pkg-config-${pkgconfig_version}.${pkgconfig_extension}"

# openssl
openssl_version="1.0.2o"
openssl_checksum="a47faaca57b47a0d9d5fb085545857cc92062691"
openssl_url="https://www.openssl.org/source"
openssl_extension="tar.gz"
openssl_filename="openssl-${openssl_version}.${openssl_extension}"

# globus
globus_version="6.0.1506371041"
globus_checksum="8cf10c2279a0598c32de517844958fc4bd3966fe"
globus_url="http://toolkit.globus.org/ftppub/gt6/installers/src/"
globus_extension="tar.gz"
globus_filename="globus_toolkit-${globus_version}.${globus_extension}"

# get current directory
CWD=`pwd`

#
# setup build environment
#

# set environment
export GLOBUS_LOCATION=/opt/ldg
export PATH=/bin:/usr/bin:/usr/sbin

# packaging location
PACKAGING_DIR=${CWD}/../destroot/ldg

# check if previous build present and remove
if [ -d ${PACKAGING_DIR} ]
then
  echo "previous build present, deleting..."
  rm -rf ${PACKAGING_DIR} || exit ${?}
fi

# check that $GLOBUS_LOCATION is empty
if [[ -d ${GLOBUS_LOCATION} ]]; then
  if [ "$(ls -A ${GLOBUS_LOCATION})" ]; then
    echo "${GLOBUS_LOCATION} is not empty"
    exit 1
  fi
fi

#
# download required tarballs
#

# change to tarball directory
pushd tarballs || exit ${?}

# download
download_and_checksum ${pkgconfig_url} ${pkgconfig_filename} ${pkgconfig_checksum}
download_and_checksum ${openssl_url} ${openssl_filename} ${openssl_checksum}
download_and_checksum ${globus_url} ${globus_filename} ${globus_checksum}

#
# build
#

# pkg-config
tar xf ${pkgconfig_filename} || exit ${?}
pushd pkg-config-${pkgconfig_version} || exit ${?}
./configure --prefix=${CWD}/autotools \
  --with-internal-glib || exit ${?}
make || exit ${?}
make install || exit ${?}
popd || exit ${?}
rm -rf pkg-config-${pkgconfig_version} || exit ${?}

# add to PATH
export PATH=${CWD}/autotools/bin:${PATH}
export PKG_CONFIG_PATH=/usr/lib/pkgconfig

# openssl
tar xf ${openssl_filename} || exit ${?}
pushd openssl-${openssl_version} || exit ${?}
./Configure \
  --prefix=${GLOBUS_LOCATION} \
  --openssldir=${GLOBUS_LOCATION}/etc/openssl \
  shared \
  darwin64-x86_64-cc || exit ${?}
make || exit ${?}
make install_sw || exit ${?}
popd || exit ${?}
rm -rf openssl-${openssl_version} || exit ${?}

# add to PKG_CONFIG_PATH
export PKG_CONFIG_PATH=${GLOBUS_LOCATION}/lib/pkgconfig:${PKG_CONFIG_PATH}

# cleanup, only libraries and headers needed
pushd ${GLOBUS_LOCATION} || exit ${?}
rm -rf bin || exit ${?}
popd

#
# globus toolkit
#

# extract source
tar xf ${globus_filename} || exit ${?}
pushd globus_toolkit-${globus_version} || exit ${?}

# patch environment script
patch -p0 < ${CWD}/patches/globus-user-env.diff || exit ${?}

# configure
./configure --prefix=${GLOBUS_LOCATION} \
  --disable-gsi-openssh || exit ${?}

# build
make || exit ${?}

# install
make install || exit ${?}

#
# myproxy
#

# change directory to myproxy source
pushd myproxy/source || exit ${?}

# configure myproxy with appropriate options
PKG_CONFIG_PATH=${GLOBUS_LOCATION}/lib/pkgconfig:$PKG_CONFIG_PATH \
./configure --prefix=${GLOBUS_LOCATION} \
  --with-sasl2=/usr --with-kerberos5=/usr || exit ${?}

# build
make || exit ${?}

# install
make install || exit ${?}

# leave directory
popd

#
# gsi-openssh
#

# change directory to gsi-openssh source
pushd gsi_openssh/source || exit ${?}

# configure gsi-openssh with appropriate options
GLOBUS_LOCATION=/opt/ldg \
PKG_CONFIG_PATH=${GLOBUS_LOCATION}/lib/pkgconfig:$PKG_CONFIG_PATH \
PATH=${GLOBUS_LOCATION}/bin:${PATH} ./configure.gnu \
  --prefix=${GLOBUS_LOCATION} \
  --without-openssl-header-check \
  --disable-option-checking \
  --enable-ltdl-install \
  --cache-file=/dev/null \
  --srcdir=. \
  --sysconfdir=${GLOBUS_LOCATION}/etc/gsissh  || exit ${?}

# build
make ||exit ${?}

# install
make install || exit ${?}

# remove ssh configuration files
rm -rf ${GLOBUS_LOCATION}/etc/gsissh/* || exit ${?}

# setup configuration location symlink
ln -s ${GLOBUS_LOCATION}/etc/gsissh ${GLOBUS_LOCATION}/etc/ssh || exit ${?}

# remove startup scripts
rm -r ${GLOBUS_LOCATION}/etc/init.d || exit ${?}

# leave directory
popd  || exit${?}

#
# cleanup
#

# leave globus source directory
popd || exit ${?}

# remove source
rm -rf globus_toolkit-${globus_version} || exit ${?}

# leave directory
popd || exit ${?}

#
# general cleanups
#

# add environment scripts to /etc
mv ${GLOBUS_LOCATION}/share/globus-user-env.csh ${GLOBUS_LOCATION}/etc/ || exit ${?}
mv ${GLOBUS_LOCATION}/share/globus-user-env.sh ${GLOBUS_LOCATION}/etc/ || exit ${?}

#
# packaging
#

# move to packaging location
mkdir -p ${PACKAGING_DIR} || exit ${?}
mv ${GLOBUS_LOCATION}/* ${PACKAGING_DIR}/ || exit ${?}

# remove build products
rm -rf autotools || exit ${?}

# done
exit 0
